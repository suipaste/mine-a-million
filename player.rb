require './mine.rb'
require './lorry.rb'
require './road.rb'

class Player

  attr_accessor :has_taken_a_turn, :pounds, :dollars

  def initialize id, name
    # Instance variables
    @id = id
    @name = name
    @pounds = 10000
    @dollars = 0
    @mine = Mine.new(id)
    @canal_title_deed = false
    @has_taken_a_turn = false
  end

  def name
    @name
  end

  def mine
    @mine
  end

  def print_stats
    puts "£#{@pounds}"
    puts "$#{@dollars}"
    puts "Units at mine #{@mine.production_units.length}"
    puts "Lorries at mine #{@mine.lorries.length}"
  end

  def hire_lorry lorries_in_pool
    if lorries_in_pool == 0
      puts "There aren't any lorries avaliable to hire."
    elsif @pounds < Lorry::HIRE_PRICE
      puts "#{@name} does not have enough money to hire a lorry."
    elsif @mine.production_units == 0
      puts "#{@name} does not have any production units to load. You need at least 1 avaliable."
    else
      @pounds -= Lorry::HIRE_PRICE
      @mine.load_lorry
    end
  end

  def sell_production_units
    if @mine.production_units.length > 0
      puts "Production units avaliable to sell:"
      puts "At mine - #{@mine.productionUnits.length} units with a value of £#{ProductionUnit::VALUE_AT_MINE} each"

      input_valid = false
      until input_valid
        puts "\nHow many production units would you like to sell?"
        input = gets.chomp

        if input.to_i == 0
          return
        elsif input.to_i.between?(1, @mine.production_units.length)
          input_valid = true

          input.to_i.times do
            @mine.production_units.pop
            @pounds += ProductionUnit::VALUE_AT_MINE
          end
        else
          puts "\nInvalid choice"
        end
      end
    else
      puts "You have no production units avaliable to sell"
    end
  end

  def claim_tax_rebate die_value
    if die_value.between?(2, 5)
      puts "You currently have no transport and did not produce production. Claim a tax rebate of £#{Game.TAX_REBATE_VALUE}."
      @pounds += Game.TAX_REBATE_VALUE
    end
  end

  def purchase_additional_working
    puts "An additional working costs #{Mine::ADDITIONAL_WORKING_COST} and will allow a mine to produce 3 units rather than 1."

    input_valid = false
    until input_valid
      puts "Would you like to purchase an additional working? (Y)es / (N)o"
      input = gets.chomp

      case input.upcase
      when "Y"
        input_valid = true
        if @pounds >= Mine::ADDITIONAL_WORKING_COST
          @pounds -= Mine::ADDITIONAL_WORKING_COST
          @mine.working += 1
        else
          puts "You don't have enough money to purchase a second working"
        end
      when "N"
        input_valid = true
      else
        puts "\nInvalid choice"
      end
    end
  end

  def has_transport? road
    return @mine.has_transport? || road.player_has_transport?(@id)? true : false
  end
end
