require './productionCard.rb'

class ProductionDeck
  def initialize()
    @cards = Array.new

    @cards.push(
      ProductionCard.new(
        "Loading bay fire",
        "All your lorries and contents or barges and contents still at the mine are lost. Return the lorries or barges to the Pool. Production Units not in lorries or barges are not lost."
      )
    )

    @cards.push(
      ProductionCard.new(
        "Hurricane!",
        "Half of the stocks at your mine, on the ground or in transport, are lost. If you have other players' units in a barge (waiting to leave) you can decide which units are lost. If any are taken out of a barge the barge must be returned to the pool and other players' units returned to them, (Players with odd numbers keep the odd one.)"
      )
    )

    @cards.shuffle
  end

  def view_next_card
    puts "-- " + @cards.first.title + " --"
    puts @cards.first.text
  end
end
