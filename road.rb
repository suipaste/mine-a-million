class Road
  def initialize()
    # Instance variables
    @nodes = Array.new(10)
  end

  def player_has_transport? player_ID
    @nodes.each do |each_node|
      if !each_node.nil? && owner == player_ID
        return true
      end

      return false
    end
  end
end
