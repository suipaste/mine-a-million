require './productionUnit.rb'

class Mine

  ADDITIONAL_WORKING_COST = 50000
  attr_accessor :production_units, :lorries

  def initialize(owner)
    # Instance variables
    @owner = owner
    @workings = 1
    @production_units = Array.new
    @lorries = Array.new
  end

  def mine_for_production die_value
    if die_value == 1 || die_value == 6
      units_produced = 1

      if @working == 2
        units_produced = 3
      end

      puts "Mine produces #{units_produced} production units"
      units_produced.times { @production_units.push(ProductionUnit.new(@owner)) }
    end
  end

  def initial_mine_for_production die_value
    units_to_claim = 0

    case die_value
    when 1..2
      units_to_claim = 2
    when 3..4
      units_to_claim = 3
    when 5..6
      units_to_claim = 4
    end

    puts "Mine produces #{units_to_claim} production unit"
    units_to_claim.times{ @production_units.push(ProductionUnit.new(@owner)) }
  end

  def load_lorry()
    production_units_to_load = 0

    if @production_units.length > 1
      input_valid = false
      until input_valid
        puts "\nYou have #{@production_units.length} units avaliable at your mine. A lorry can carry up to 2 but cannot be empty, how many would you like to load?"
        input = gets.chomp

        case input
        when "1"
          input_valid = true
        when "2"
          input_valid = true
        else
          puts "\nInvalid choice"
        end
        production_units_to_load = input.to_i
      end
    end

    lorry = Lorry.new(@owner)
    production_units_to_load.times { lorry.production_units.push(@production_units.pop) }
    @lorries.push(lorry)
    puts "Lorry hired and loaded with #{production_units_to_load} production units"
  end

  def has_transport?
    return has_lorries? ? true : false
  end

  def has_lorries?
    return @lorries.empty? ? false : true
  end
end
