class Dice
  def initialize(sides)
    # Instance variables
    @sides = sides
  end

  def roll
    @current_value = Random.new.rand(1..@sides)
    puts "Rolled a #{@current_value}"
  end

  def current_value
    @current_value
  end
end
