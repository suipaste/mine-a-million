require './player.rb'

class Players
  def initialize()
    # Instance variables
    @players = Array.new()
    create_players(input_number_of_players)
  end

  def players
    @players
  end

  def input_number_of_players
    input_valid = false

    until input_valid
      puts "How many players? (2-6)"
      input = gets.chomp

      if input.to_i.to_s == input
        # We have an integer
        num_of_players = input.to_i

        if num_of_players.between?(2, 6)
          input_valid = true
        else
          puts "\nMust be between 2 and 6"
        end
      else
        puts "\nMust be a whole number"
      end
    end

    num_of_players
  end

  def create_players(num_of_players)
    for i in 1..num_of_players
      input_valid = false;

      while !input_valid
        puts "\nEnter player #{i}'s name"
        input = gets.chomp

        if input.empty?
          puts "Player must have a name"
        else
          input_valid = true
        end
      end

      @players.push(Player.new(i, input))
    end
  end
end
