class ProductionUnit

  # Domestic value
  VALUE_AT_MINE     = 5000
  VALUE_AT_NEWPORT  = 20000

  # Export value
  VALUE_AT_ST_JOHN = 40000
  VALUE_AT_RACE_BAY = 40000
  VALUE_AT_PORT_BOSTON = 80000

  def initialize(owner)
    # Instance variables
    @owner = owner
  end
end
