class ProductionCard
  def initialize(title, text)
    @title = title
    @text = text
  end

  def title
    @title
  end

  def text
    @text
  end
end
