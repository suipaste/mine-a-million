require './dice.rb'
require './players.rb'
require './productionDeck.rb'
require './road.rb'

class Game

  TAX_REBATE_VALUE = 2500

  def initialize()
    input_valid = false

    while !input_valid
      puts "(N)ew game, (Q)uit"
      input = gets.chomp

      case input.upcase
      when "N"
        input_valid = true
      when "Q"
        exit
      else
        puts "\nInvalid choice"
      end
    end
  end

  def players
    @players
  end

  def cards
    @cards
  end

  def die
    @die
  end

  def road
    @road
  end

  def lorries_in_pool
    @lorries_in_pool
  end

  def setup
    puts "\nSetting up a new game...\n"
    @players        = Players.new
    @die            = Dice.new(6)
    @cards          = ProductionDeck.new
    @road           = Road.new
    @lorries_in_pool  = 6
  end

  def play()
    game_complete = false
    first_round_complete = false

    puts "\nStarting game..."

    while !game_complete do
      @players.players.each do |each_player|
        puts "\nIt's " + each_player.name + "'s turn"
        take_turn(each_player)
      end
    end
  end

  def take_turn(player)
    if player.has_taken_a_turn
      normal_turn(player)
    else
      initial_turn(player)
    end
  end

  def normal_turn(player)

    end_turn = false

    until end_turn
      input_valid = false
      until input_valid
        player.print_stats
        puts "\n(V)iew next production card | (S)ell production units | Hire (L)orry £ #{Lorry::HIRE_PRICE} | (R)oll"
        input = gets.chomp

        case input.upcase
        when "V"
          input_valid = true
          @cards.view_next_card
        when "S"
          input_valid = true
          player.sell_production_units
        when "L"
          input_valid = true
          player.hire_lorry(@lorries_in_pool)
        when "R"
          input_valid = true
          @die.roll

          # TODO take a production card
          player.mine.mine_for_production(@die.current_value)

          if player.has_transport?(@road)
            avaliable_transport_msg = "You have transport in play"

            transport_on_road = false
            transport_at_mine = false
            if @road.player_has_transport?(player.id)
              transport_on_road = true
            end

            if player.mine.has_transport?
              transport_at_mine = true
            end
          else
            player.claim_tax_rebate
          end

          end_turn = true
        else
          puts "\nInvalid choice"
        end
      end
    end
  end

  def getAllowableActions(player)
    can_hire_lorry          = false
    can_hire_barge          = false
    can_hire_ship           = false
    can_buy_canal_title_deed  = false
    can_buy_second_working   = false
    can_sell_production     = false

    if @lorries_in_pool > 0 && player.pounds >= Lorry.HIRE_PRICE && player.mine.production_units > 0
      can_hire_lorry = true
    end


  end


  def initial_turn(player)
    input_valid = false

    while !input_valid
      puts "\n(R)oll for production"
      input = gets.chomp

      if "R".casecmp(input) == 0
        input_valid = true
      else
        puts "\nInvalid choice"
      end
    end

    @die.roll
    player.mine.initial_mine_for_production(@die.current_value)
    player.has_taken_a_turn = true
  end




  puts "******************"
  puts "* Mine-A-Million *"
  puts "******************"
  puts ""
  game = Game.new()
  game.setup
  game.play


end
